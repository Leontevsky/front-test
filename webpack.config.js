const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: "./src/js/tabs.js",
    output: {
        filename: "js/scripts.min.js",
        path: path.resolve(__dirname, "dist"),
    },

    devtool: 'source-map',
    optimization: {
        // разобраться с чанками
    },

    module: {
        rules: [
          {
            test: /\.s[ac]ss$/i,
            use: [
                MiniCssExtractPlugin.loader,

                {
                    loader: "css-loader",
                    options: {
                      sourceMap: true,
                    },
                  },
                'sass-loader',
            ],
          },
          {
            test: /\.(png|jpe?g|gif|svg)$/i,
            use: [
              {
                loader: 'file-loader',
              },
            ],
          },
          {
              test: /\.js$/i,
              exclude: /node_modules/,
              use: [
                  {
                    loader: 'babel-loader',
                    options: {
                        sourceMap: true,
                      },
                  }
              ]
          }
        ],
      },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: './css/styles.min.css',
        }),
    ]
};


